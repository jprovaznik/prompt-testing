# frozen_string_literal: true

require 'carrierwave/orm/activerecord'

class Project < ApplicationRecord
  has_many :project_topics, -> { order(:id) }, class_name: 'Projects::ProjectTopic'
  has_many :topics, through: :project_topics, class_name: 'Projects::Topic'

  attr_accessor :old_path_with_namespace
  attr_accessor :template_name
  attr_writer :pipeline_status
  attr_accessor :skip_disk_validation
  attr_writer :topic_list

  alias_attribute :title, :name

  # Relations
  belongs_to :pool_repository
  belongs_to :creator, class_name: 'User'
  belongs_to :organization, class_name: 'Organizations::Organization'
  belongs_to :group, -> { where(type: Group.sti_name) }, foreign_key: 'namespace_id'
  belongs_to :namespace
  # Sync deletion via DB Trigger to ensure we do not have
  # a project without a project_namespace (or vice-versa)
  belongs_to :project_namespace, autosave: true, class_name: 'Namespaces::ProjectNamespace', foreign_key: 'project_namespace_id', inverse_of: :project
  alias_method :parent, :namespace
  alias_attribute :parent_id, :namespace_id

  has_one :catalog_resource, class_name: 'Ci::Catalog::Resource', inverse_of: :project
  has_many :ci_components, class_name: 'Ci::Catalog::Resources::Component', inverse_of: :project
  has_many :catalog_resource_versions, class_name: 'Ci::Catalog::Resources::Version', inverse_of: :project

  has_one :last_event, -> { order 'events.created_at DESC' }, class_name: 'Event'
  has_many :boards

  # Projects with a very large number of notes may time out destroying them
  # through the foreign key. Additionally, the deprecated attachment uploader
  # for notes requires us to use dependent: :destroy to avoid orphaning uploaded
  # files.
  #
  # https://gitlab.com/gitlab-org/gitlab/-/issues/207222
  # Order of this association is important for project deletion.
  # has_many :notes` should be the first association among all `has_many` associations.
  has_many :notes, dependent: :destroy # rubocop:disable Cop/ActiveRecordDependent

  has_many :forked_to_members, class_name: 'ForkNetworkMember', foreign_key: 'forked_from_project_id'
  has_many :forks, through: :forked_to_members, source: :project, inverse_of: :forked_from_project
  has_many :fork_network_projects, through: :fork_network, source: :projects

  has_many :project_authorizations
  has_many :authorized_users, -> { allow_cross_joins_across_databases(url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/422045') },
    through: :project_authorizations, source: :user, class_name: 'User'

  scope :created_by, -> (user) { where(creator: user) }
  scope :imported_from, -> (type) { where(import_type: type) }
  scope :imported, -> { where.not(import_type: nil) }
  scope :with_enabled_error_tracking, -> { joins(:error_tracking_setting).where(project_error_tracking_settings: { enabled: true }) }
  scope :last_activity_before, -> (time) { where('projects.last_activity_at < ?', time) }

  scope :with_service_desk_key, -> (key) do
    # project_key is not indexed for now
    # see https://gitlab.com/gitlab-org/gitlab/-/merge_requests/24063#note_282435524 for details
    joins(:service_desk_setting).where('service_desk_settings.project_key' => key)
  end

  scope :with_topic, ->(topic) { where(id: topic.project_topics.select(:project_id)) }

  scope :with_topic_by_name, ->(topic_name) do
    topic = Projects::Topic.find_by_name(topic_name)

    topic ? with_topic(topic) : none
  end

  scope :pending_data_repair_analysis, -> do
    left_outer_joins(:container_registry_data_repair_detail)
    .where(container_registry_data_repair_details: { project_id: nil })
    .order(id: :desc)
  end

  enum auto_cancel_pending_pipelines: { disabled: 0, enabled: 1 }

  chronic_duration_attr :build_timeout_human_readable, :build_timeout,
    default: 3600, error_message: N_('Maximum job timeout has a value which could not be accepted')

  validates :build_timeout, allow_nil: true, numericality: {
    greater_than_or_equal_to: 10.minutes,
    less_than: MAX_BUILD_TIMEOUT,
    only_integer: true,
    message: N_('needs to be between 10 minutes and 1 month')
  }

  # Used by Projects::CleanupService to hold a map of rewritten object IDs
  mount_uploader :bfg_object_map, AttachmentUploader

  def self.with_api_entity_associations
    preload<cursor-1>(:project_feature, :route, :topics, :group, :timelogs, namespace: [:route, :owner])
  end

  def self.with_feature_available_for_user(feature, user)
    with_project_feature.merge(ProjectFeature.with_feature_available_for_user(feature, user))
  end

  class << self
    # class method which searches for a project by its name<cursor-2>
    def search(name)
      if include_namespace
        joins(:route).fuzzy_search(query, [Route.arel_table[:path], Route.arel_table[:name], :description],
          use_minimum_char_limit: use_minimum_char_limit)
        .allow_cross_joins_across_databases(url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/421843')
      else
        fuzzy_search(query, [:path, :name, :description], use_minimum_char_limit: use_minimum_char_limit)
      end
    end

    def search_by_title<cursor-3>(query)
      non_archived.fuzzy_search(query, [:name])
    end
  end

  def initialize(attributes = nil)
    # We assign the actual snippet default if no explicit visibility has been initialized.
    attributes ||= {}

    unless visibility_attribute_present?(attributes)
      attributes[:visibility_level] = Gitlab::CurrentSettings.default_project_visibility
    end

    @init_attributes = attributes

    super
  end
end
