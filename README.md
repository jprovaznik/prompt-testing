# Prompt Testing

The purpose of this project is to gather various snippets of source code
(including expected suggestion) which can be used for testing and improving
code suggestions for GitLab project and also provide an easy way to run code
suggestions on these snippets and check differences against expected result.

It "emulates" client's editor, for example if you add a file "example.rb" with this content:

```ruby
def hello_world
  puts "hello <cursor-1>world"
end

# create a method which prints all numbers
# from 1 to 10<cursor-2>
```

Then it will run two code suggestion requests (for `cursor-1`, and `cursor-2`),
for each of them it splits file content into parameters sent from vscode client
(`content_above_cursor`, `content_below_cursor`, ...).

## How to use it

- set up [code suggestions](https://docs.gitlab.com/ee/development/code_suggestions/#setup-instructions)
- clone this repository
- run `RAILS_TOKEN=<your local  RAILS_URL=http://<your local gdk host>:3000/api/v4/code_suggestions/completions ruby run.rb`

## How to add new code snippet

- create a file (source code file for any supported language) under `snippets` directory (e.g. `snippets/project.py`)
- edit the file and place `<cursor-X>` placeholders (where X is a number) to any places in the file where you want to emulate curcor placement in the client
- create a file with expected response for each cursor with name `<filename>.cursor-X`, e.g. `snippets/project.py.cursor-1`)
