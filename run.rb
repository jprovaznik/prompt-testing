#!/usr/bin/env ruby
# frozen_string_literal: true

require 'find'
require 'logger'
require 'net/http'
require 'json'

class PromptRunner
  CURSOR_REGEXP = /<cursor-(?<num>\d+)>/
  DELAY = 0.5

  def initialize(dir:)
    @snippet_dir = dir
    @logger = Logger.new(STDOUT)
  end

  def run
    prompt_files.each do |filename|
      prompts(filename).each do |prompt|
        response = request(prompt)
        compare_response(prompt, response)
        sleep(DELAY)
      end
    end
  end

  private

  attr_reader :snippet_dir, :logger

  def rails_url
    # e.g. http://127.0.0.1:3000/api/v4/code_suggestions/completions
    raise "RAILS_URL env variable is not set" unless ENV['RAILS_URL']

    ENV['RAILS_URL']
  end

  def rails_token
    raise "RAILS_TOKEN env variable is not set" unless ENV['RAILS_TOKEN']

    ENV['RAILS_TOKEN']
  end

  def prompt_files
    files = []

    Find.find(snippet_dir) do |path|
      next unless File.file?(path)
      next if File.basename(path).start_with?('.')
      next if File.extname(path) =~ /result/

      files << path
    end

    files
  end

  def prompts(filename)
    matches = []
    File.read(filename).scan(CURSOR_REGEXP) { matches << Regexp.last_match }

    matches.map do |match|
      {
        filename: filename,
        cursor_id: match[:num],
        above: match.pre_match.gsub(CURSOR_REGEXP, ''),
        below: match.post_match.gsub(CURSOR_REGEXP, '')
      }
    end
  end

  def request(prompt)
    logger.info "path: #{prompt[:filename]}, cursor: #{prompt[:cursor_id]}"

    headers = {
      "PRIVATE-TOKEN" => rails_token,
      "Content-Type" => "application/json"
    }

    data = {
      current_file: {
        file_name: prompt[:filename],
        content_above_cursor: prompt[:above],
        content_below_cursor: prompt[:below]
      }
    }.to_json

    t0 = Time.now.to_f
    res = Net::HTTP.post URI(rails_url), data, headers
    logger.info "duration: #{Time.now.to_f - t0}"

    res
  end

  def compare_response(prompt, response)
    unless response.code == '200'
      logger.error "response code is #{response.code}, body: #{response.body}"
      return
    end

    data = JSON.parse(response.body)
    unless data['choices']&.first
      logger.error "choices are missing, body: #{response.body}"
      return
    end

    text = data['choices']&.first['text']

    logger.info <<~RESPONSE

      <<< expected response:
      #{expected_content(prompt)}
      >>> real response:
      #{text}
    RESPONSE
  end

  def expected_content(prompt)
    filename = "#{prompt[:filename]}.cursor-#{prompt[:cursor_id]}"

    unless File.exists?(filename)
      logger.error "missing expected response file #{filename} for #{prompt[:filename]}, cursor: #{prompt[:cursor_id]}"
      return
    end

    File.read(filename)
  end
end

PromptRunner.new(dir: 'snippets').run
